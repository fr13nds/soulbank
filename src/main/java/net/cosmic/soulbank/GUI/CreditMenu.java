package net.cosmic.soulbank.GUI;

import mc.obliviate.inventory.Gui;
import mc.obliviate.inventory.Icon;
import net.cosmic.soulbank.FileConfig.FileConfigManager;
import net.cosmic.soulbank.SoulBank;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

import static net.cosmic.soulbank.Utils.SkullCreator.createRealPlayerSkull;

public class CreditMenu extends Gui {

    private final SoulBank plugin = SoulBank.getInstance();

    public CreditMenu(Player player) {
        super(player, "creditMenu", "Kredite", 3);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
       
        Iterable<Integer> iterable = Arrays.asList(4, 10, 12, 14, 16, 18, 26);

        fillGui(new Icon(Material.BLACK_STAINED_GLASS_PANE).setName(" "), iterable);

        Icon ironCredit = new Icon(Material.IRON_BLOCK).setName("§7Eisen Kredit").setLore("§7Klicke hier um einen Kredit von 5000€ aufzunehmen!");
        Icon goldCredit = new Icon(Material.GOLD_BLOCK).setName("§6Gold Kredit").setLore("§7Klicke hier um einen Kredit von 10000€ aufzunehmen!");
        Icon redstoneCredit = new Icon(Material.REDSTONE_BLOCK).setName("§cRedstone Kredit").setLore("§7Klicke hier um einen Kredit von 25000€ aufzunehmen!");
        Icon diamondCredit = new Icon(Material.DIAMOND_BLOCK).setName("§bDiamant Kredit").setLore("§7Klicke hier um einen Kredit von 50000€ aufzunehmen!");
        ItemStack skull = createRealPlayerSkull(player, "§aDu besitzt in deiner Geldbörse " + plugin.getFileConfigManager().getPurseMoney(player) + "§a€", "§aDu besitzt auf deinem Konto " + plugin.getFileConfigManager().getBankMoney(player) + "§a€");
        Icon back = new Icon(Material.BARRIER).setName("§cZurück").setLore("§7Klicke hier um zurück zum Hauptmenü zu gelangen");


        Icon ironCreditNotAvailable = new Icon(Material.IRON_BLOCK).setName("§7Eisen Kredit").setLore("§cDu hast einen Kredit bereits aufgenommen!");
        Icon goldCreditNotAvailable = new Icon(Material.GOLD_BLOCK).setName("§6Gold Kredit").setLore("§cDu hast einen Kredit bereits aufgenommen!");
        Icon redstoneCreditNotAvailable = new Icon(Material.REDSTONE_BLOCK).setName("§cRedstone Kredit").setLore("§cDu hast einen Kredit bereits aufgenommen!");
        Icon diamondCreditNotAvailable = new Icon(Material.DIAMOND_BLOCK).setName("§bDiamant Kredit").setLore("§cDu hast einen Kredit bereits aufgenommen!");

        long creditAmount = plugin.getFileConfigManager().calculateCredit(player);

        Icon ironCreditGot = new Icon(Material.IRON_BLOCK).setName("§7Eisen Kredit").setLore("§aKlicke um diesen Kredit zurück zuzahlen", "§7Du musst " + creditAmount + "€ zahlen!");
        Icon goldCreditGot = new Icon(Material.GOLD_BLOCK).setName("§6Gold Kredit").setLore("§aKlicke um diesen Kredit zurück zuzahlen", "§7Du musst " + creditAmount + "€ zahlen!");
        Icon redstoneCreditGot = new Icon(Material.REDSTONE_BLOCK).setName("§cRedstone Kredit").setLore("§aKlicke um diesen Kredit zurück zuzahlen", "§7Du musst " + creditAmount + "€ zahlen!");
        Icon diamondCreditGot = new Icon(Material.DIAMOND_BLOCK).setName("§bDiamant Kredit").setLore("§aKlicke um diesen Kredit zurück zuzahlen", "§7Du musst " + creditAmount + "€ zahlen!");

        Icon info = new Icon(Material.OAK_SIGN).setName("§b§lInfo").setLore("§7Hier kannst du einen Kredit aufnehmen!", "§7Du hast 3 Tage Zeit diesen zurück zu zahlen!",
                "§7Es werden jede Stunde §c1% §7Zinsen auf den Kredit berechnet!",
                "§7Wenn du den Kredit nicht zurück zahlst, wird dein Account eingefrohren!",
                "§7Du kannst dann kein Geld mehr einzahlen oder auszahlen!",
                "§7Zahle den Kredit zurück um deinen Account zu entsperren!");

        switch (plugin.getFileConfigManager().getCreditType(player)) {

            case "IRON":
                addItem(10, ironCreditGot);
                addItem(12, goldCreditNotAvailable);
                addItem(14, redstoneCreditNotAvailable);
                addItem(16, diamondCreditNotAvailable);

                break;
            case "GOLD":
                addItem(10, ironCreditNotAvailable);
                addItem(12, goldCreditGot);
                addItem(14, redstoneCreditNotAvailable);
                addItem(16, diamondCreditNotAvailable);

                break;
            case "REDSTONE":
                addItem(10, ironCreditNotAvailable);
                addItem(12, goldCreditNotAvailable);
                addItem(14, redstoneCreditGot);
                addItem(16, diamondCreditNotAvailable);

                break;
            case "DIAMOND":
                addItem(10, ironCreditNotAvailable);
                addItem(12, goldCreditNotAvailable);
                addItem(14, redstoneCreditNotAvailable);
                addItem(16, diamondCreditGot);

                break;
            case "NONE":
                addItem(10, ironCredit);
                addItem(12, goldCredit);
                addItem(14, redstoneCredit);
                addItem(16, diamondCredit);

        }

        addItem(4, info);
        addItem(18, back);
        addItem(26, skull);

        back.onClick(event1 -> {
            new MainMenu(player).open();
        });

        //Get Credit
        ironCredit.onClick(event1 -> {
            plugin.getFileConfigManager().setCredit(player, FileConfigManager.Credit.IRON);
            new CreditMenu(player).open();
        });

        goldCredit.onClick(event1 -> {
            plugin.getFileConfigManager().setCredit(player, FileConfigManager.Credit.GOLD);
            new CreditMenu(player).open();
        });

        redstoneCredit.onClick(event1 -> {
            plugin.getFileConfigManager().setCredit(player, FileConfigManager.Credit.REDSTONE);
            new CreditMenu(player).open();
        });

        diamondCredit.onClick(event1 -> {
            plugin.getFileConfigManager().setCredit(player, FileConfigManager.Credit.DIAMOND);
            new CreditMenu(player).open();
        });

        //Pay Back Credit
        ironCreditGot.onClick(event1 -> {
            plugin.getFileConfigManager().payBackCredit(player);
            new CreditMenu(player).open();
        });

        goldCreditGot.onClick(event1 -> {
            plugin.getFileConfigManager().payBackCredit(player);
            new CreditMenu(player).open();
        });

        redstoneCreditGot.onClick(event1 -> {
            plugin.getFileConfigManager().payBackCredit(player);
            new CreditMenu(player).open();
        });

        diamondCreditGot.onClick(event1 -> {
            plugin.getFileConfigManager().payBackCredit(player);
            new CreditMenu(player).open();
        });


    }


}