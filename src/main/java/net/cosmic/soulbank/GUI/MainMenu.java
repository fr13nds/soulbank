package net.cosmic.soulbank.GUI;

import mc.obliviate.inventory.Gui;
import mc.obliviate.inventory.Icon;
import net.cosmic.soulbank.SoulBank;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Evoker;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.profile.PlayerProfile;

import java.util.*;

import static net.cosmic.soulbank.Utils.SkullCreator.createRealPlayerSkull;


public class MainMenu extends Gui {

    SoulBank plugin = SoulBank.getInstance();

    public MainMenu(Player player) {
        super(player, "soulbank", "SoulBank", 3);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
        Player player = (Player) event.getPlayer();


        Iterable<Integer> iterable = Arrays.asList(11, 13, 15, 26);
        fillGui(new Icon(Material.BLACK_STAINED_GLASS_PANE).setName(" "), iterable);

        Icon deposit = new Icon(Material.HOPPER).setName("§aEinzahlen").setLore("§7Klicke hier um Geld einzuzahlen");
        Icon withdraw = new Icon(Material.DROPPER).setName("§cAuszahlen").setLore("§7Klicke hier um Geld auszuzahlen");
        Icon credit = new Icon(Material.PAPER).setName("§eKredit").setLore("§7Klicke hier um einen Kredit aufzunehmen");
        ItemStack skull = createRealPlayerSkull(player, "§aDu besitzt in deiner Geldbörse " + plugin.getFileConfigManager().getPurseMoney(player) + "§a€", "§aDu besitzt auf deinem Konto " + plugin.getFileConfigManager().getBankMoney(player) + "§a€");

        Icon depositFrozen = new Icon(Material.HOPPER).setName("§aEinzahlen").setLore("§cDu kannst derzeit kein Geld einzahlen, da dein Account eingefrohren ist!", "§cZahle dein Kredit zurück um dein Account zu entsperren!");
        Icon withdrawFrozen = new Icon(Material.DROPPER).setName("§cAuszahlen").setLore("§cDu kannst derzeit kein Geld auszahlen, da dein Account eingefrohren ist!", "§cZahle dein Kredit zurück um dein Account zu entsperren!");

        if(plugin.getFileConfigManager().isAccountFrozen(player)){
            addItem(11, depositFrozen);
            addItem(13, withdrawFrozen);

        }else {
            addItem(11, deposit);
            addItem(13, withdraw);
        }


        addItem(15, credit);
        addItem(26, skull);

        deposit.onClick(event1 -> {
            new DepositMenu(player).open();
        });

        withdraw.onClick(event1 -> {
            new WithdrawMenu(player).open();
        });

        credit.onClick(event1 -> {
          new CreditMenu(player).open();
        });

    }


}

