package net.cosmic.soulbank.GUI;

import io.github.rapha149.signgui.SignGUI;
import io.github.rapha149.signgui.SignGUIAction;
import mc.obliviate.inventory.Gui;
import mc.obliviate.inventory.Icon;
import net.cosmic.soulbank.SoulBank;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.profile.PlayerProfile;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

import static net.cosmic.soulbank.Utils.SkullCreator.createRealPlayerSkull;

public class WithdrawMenu extends Gui {
    SoulBank plugin = SoulBank.getInstance();


    public WithdrawMenu(Player player) {
        super(player, "withdrawMenu", "Abheben", 3);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
        Icon withdrawCustom = new Icon(Material.OAK_SIGN).setName("§cAuszahlen").setLore("§7Klicke hier um eine bestimmte Menge einzuzahlen");
        Icon back = new Icon(Material.BARRIER).setName("§cZurück").setLore("§7Klicke hier um zurück zum Hauptmenü zu gelangen");
        Icon withdrawFull = new Icon(Material.DROPPER).setName("§cAuszahlen").setLore("§7Klicke hier um all dein Geld auszuzahlen");
        Icon withdrawHalf = new Icon(Material.DROPPER).setName("§cAuszahlen").setLore("§7Klicke hier um die Hälfte deines Geldes auszuzahlen");


        ItemStack skull = createRealPlayerSkull(player, "§aDu besitzt in deiner Geldbörse " + plugin.getFileConfigManager().getPurseMoney(player) + "§a€", "§aDu besitzt auf deinem Konto " + plugin.getFileConfigManager().getBankMoney(player) + "§a€");

        Iterable<Integer> iterable = Arrays.asList(4, 11, 15, 18, 26);
        fillGui(new Icon(Material.BLACK_STAINED_GLASS_PANE).setName(" "), iterable);

        addItem(11, withdrawFull);
        addItem(15, withdrawHalf);
        addItem(4, withdrawCustom);
        addItem(18, back);
        addItem(26, skull);




        back.onClick(event1 -> {
            new MainMenu(player).open();
        });

        withdrawHalf.onClick(event1 -> {
            plugin.getFileConfigManager().withdrawFromBank(player, plugin.getFileConfigManager().getBankMoney(player) / 2);
            new WithdrawMenu(player).open();
        });

        withdrawFull.onClick(event1 -> {
            plugin.getFileConfigManager().withdrawFromBank(player, plugin.getFileConfigManager().getBankMoney(player));
            new WithdrawMenu(player).open();
        });

        withdrawCustom.onClick(event1 -> {
            AtomicLong amount = new AtomicLong(-1);

            SignGUI signGUI = SignGUI.builder()
                    .setLines(null, "^^^^^^^^^^^^^^^", "Gib einen Betrag ein")
                    .setType(Material.OAK_SIGN)
                    .setHandler((p, result) -> {

                        try {
                            amount.set(Long.parseLong(result.getLine(0)));
                        } catch (NumberFormatException e) {
                            player.sendMessage(plugin.getMessageManager().getOnlyNumbers());
                            return Collections.emptyList();

                        }
                        return List.of(
                                SignGUIAction.run(() -> {
                                    plugin.getFileConfigManager().withdrawFromBank(player, amount.get());
                                    new WithdrawMenu(player).open();
                                })
                        );


                    })
                    .build();

            signGUI.open(player);

        });
    }


}
