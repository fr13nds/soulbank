package net.cosmic.soulbank.GUI;

import io.github.rapha149.signgui.SignGUI;
import io.github.rapha149.signgui.SignGUIAction;
import mc.obliviate.inventory.Gui;
import mc.obliviate.inventory.Icon;
import net.cosmic.soulbank.SoulBank;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.profile.PlayerProfile;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

import static net.cosmic.soulbank.Utils.SkullCreator.createRealPlayerSkull;

public class DepositMenu extends Gui {

    SoulBank plugin = SoulBank.getInstance();


    public DepositMenu(@Nonnull Player player) {
        super(player, "deposiMenu", "Einzahlen", 3);
    }

    @Override
    public void onOpen(InventoryOpenEvent event) {
        Icon despositFull = new Icon(Material.HOPPER).setName("§aEinzahlen").setLore("§7Klicke hier um all dein Geld einzuzahlen");
        Icon depositHalf = new Icon(Material.HOPPER).setName("§aEinzahlen").setLore("§7Klicke hier um die Hälfte deines Geldes einzuzahlen");
        Icon depositCustom = new Icon(Material.OAK_SIGN).setName("§aEinzahlen").setLore("§7Klicke hier um eine bestimmte Menge einzuzahlen");
        Icon back = new Icon(Material.BARRIER).setName("§cZurück").setLore("§7Klicke hier um zurück zum Hauptmenü zu gelangen");


        ItemStack skull = createRealPlayerSkull(player, "§aDu besitzt in deiner Geldbörse " + plugin.getFileConfigManager().getPurseMoney(player) + "§a€", "§aDu besitzt auf deinem Konto " + plugin.getFileConfigManager().getBankMoney(player) + "§a€");

        Iterable<Integer> iterable = Arrays.asList(4, 11, 15, 18, 26);
        fillGui(new Icon(Material.BLACK_STAINED_GLASS_PANE).setName(" "), iterable);

        addItem(11, despositFull);
        addItem(15, depositHalf);
        addItem(4, depositCustom);
        addItem(18, back);
        addItem(26, skull);

        back.onClick(event1 -> new MainMenu(player).open());

        despositFull.onClick(event1 -> {

            plugin.getFileConfigManager().addMoneyToBank(plugin.getFileConfigManager().getPurseMoney(player), player);

            new DepositMenu(player).open();
        });

        depositHalf.onClick(event1 -> {
            long amount = plugin.getFileConfigManager().getPurseMoney(player) / 2;
            plugin.getFileConfigManager().addMoneyToBank(amount, player);

            new DepositMenu(player).open();

        });

        depositCustom.onClick(event1 -> {
            AtomicLong amount = new AtomicLong(-1);

            SignGUI signGUI = SignGUI.builder()
                    .setLines(null, "^^^^^^^^^^^^^^^", "Gib einen Betrag ein")
                    .setType(Material.OAK_SIGN)
                    .setHandler((p, result) -> {

                        try {
                            amount.set(Long.parseLong(result.getLine(0)));
                        } catch (NumberFormatException e) {
                            player.sendMessage(plugin.getMessageManager().getOnlyNumbers());
                            return Collections.emptyList();

                        }
                        return List.of(
                                SignGUIAction.run(() -> {
                                    plugin.getFileConfigManager().addMoneyToBank(amount.get(), player);
                                    new DepositMenu(player).open();
                                })
                        );


                    })
                    .build();

            signGUI.open(player);
        });


    }
}
