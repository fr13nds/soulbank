package net.cosmic.soulbank.Listeners;

import net.cosmic.soulbank.SoulBank;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class onJoin implements Listener {

    private SoulBank plugin = SoulBank.getPlugin(SoulBank.class);
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            plugin.getFileConfigManager().refreshBank(event.getPlayer());
        }, 60L); //20 Tick (1 Second) delay before run() is called

    }
}
