package net.cosmic.soulbank.FileConfig;

import com.earth2me.essentials.Essentials;
import net.cosmic.soulbank.SoulBank;
import net.ess3.api.MaxMoneyException;
import org.bukkit.entity.Player;

import java.math.BigDecimal;

public class FileConfigManager {

    private SoulBank plugin = SoulBank.getPlugin(SoulBank.class);
    private final Essentials essentials;
    private final FileConfig fileConfig;

    public FileConfigManager(Essentials essentials) {
        this.essentials = essentials;
        this.fileConfig = new FileConfig("bank.yml");

    }


    /**
     * Refreshes the bank onJoin or onPlayerInteractWithEntity Event
     *
     * @param player that should
     */
    public void refreshBank(Player player) {
        calculateFrozen(player);
        refreshBankInterest(player);

    }

    /**
     * Init the bank, if the player has never been on the server
     *
     * @param player to set the init
     */

    public void initBank(Player player) {
        if (!fileConfig.contains("players." + player.getUniqueId())) {
            setCreditInit(player);
            setPlayerBank(player, 0, true, false);
        }

    }


    private void setPlayerBank(Player player, long amount, boolean silent, boolean withdraw) {

        if (getPurseMoney(player) < amount && !withdraw) {
            player.sendMessage(plugin.getMessageManager().getNotEnoughMoney());
            return;
        }

        fileConfig.set("players." + player.getUniqueId() + ".bank", amount);
        fileConfig.set("players." + player.getUniqueId() + ".time", System.currentTimeMillis());

        fileConfig.saveConfig();
        if (!silent)
            player.sendMessage(plugin.getMessageManager().getDepositAmount(amount));

    }

    /**
     * Adds money to the players bank
     *
     * @param amount Amount of money to add
     * @param player Player to add money to
     * @throws IllegalArgumentException if the player does not have enough money in their purse
     */
    public void addMoneyToBank(long amount, Player player) {

        if (getPurseMoney(player) < amount) {
            player.sendMessage(plugin.getMessageManager().getNotEnoughMoney());
            return;
        }

        if (amount == -1) {
            player.sendMessage(plugin.getMessageManager().getOnlyNumbers());
            return;
        }

        if (amount == 0) {
            player.sendMessage(plugin.getMessageManager().getNothingToDeposit());
            return;
        }

        if (!fileConfig.contains("players." + player.getUniqueId() + ".bank")) {
            setPlayerBank(player, amount, false, false);

        }

        fileConfig.set("players." + player.getUniqueId() + ".bank", getBankMoney(player) + amount);
        fileConfig.saveConfig();

        removePurseBalance(player, amount);
        player.sendMessage(plugin.getMessageManager().getDepositAmount(amount));

    }

    /**
     * Sets silently the players bank balance
     *
     * @param player Player to set balance for
     * @param amount Amount to set, already calculated. Means that you must add the current balance to the amount you want to set
     */
    private void setMoneyToBank(Player player, long amount) {

        fileConfig.set("players." + player.getUniqueId() + ".bank", amount);
        fileConfig.saveConfig();

        assert amount == getBankMoney(player);
    }

    /**
     * Gets the players bank balance including credit
     *
     * @param player Player to get balance for
     * @return Balance of player
     */
    public long getBankMoney(Player player) {

        return fileConfig.getLong("players." + player.getUniqueId() + ".bank");
    }

    private void removeMoneyFromBank(Player player, long amount) {
        fileConfig.set("players." + player.getUniqueId() + ".bank", getBankMoney(player) - amount);
        fileConfig.saveConfig();
    }

    /**
     * Gets the time elapsed since the players first transaction
     *
     * @param player Player to get time for
     * @return Time elapsed in hours
     */
    private long getBankTimeElapsed(Player player) {
        long currentTime = System.currentTimeMillis();
        long time = fileConfig.getLong("players." + player.getUniqueId() + ".time");
        long inHours = (currentTime - time) / 3600000;
        return inHours;
    }


    public void withdrawFromBank(Player player, long amount) {
        long balance = getBankMoney(player);
        if (amount < 0) {
            player.sendMessage(plugin.getMessageManager().getOnlyNumbers());
            return;
        }

        if (amount == 0) {
            player.sendMessage(plugin.getMessageManager().getNothingToWithdraw());
            return;
        }

        if (balance >= amount) {
            addPurseBalance(player, amount);
            setPlayerBank(player, balance - amount, true, true);
            player.sendMessage(plugin.getMessageManager().getWithdrawAmount(amount));
            return;
        }

        player.sendMessage(plugin.getMessageManager().getNotEnoughMoney());

    }

    //Handle purse

    private void removePurseBalance(Player player, long amount) {
        long balance = getPurseMoney(player);
        if (balance >= amount) {
            setPlayerPurse(player, balance - amount);
        }
    }

    /**
     * Adds money to the players purse
     *
     * @param player Player to add money to
     * @param amount Amount of money to add
     * @throws IllegalArgumentException if the player does not have enough money in their bank
     */
    private void addPurseBalance(Player player, long amount) {

        setPlayerPurse(player, getPurseMoney(player) + amount);

    }


    public long getPurseMoney(Player player) {
        return essentials.getUser(player).getMoney().longValue();
    }

    private void setPlayerPurse(Player player, long amount) {
        try {
            essentials.getUser(player).setMoney(BigDecimal.valueOf(amount));
        } catch (MaxMoneyException exception) {
            exception.printStackTrace();
        }

    }


    // Interest are always fixed negative

    /**
     * Refreshes the players bank interest
     *
     * @param player Player to add interest to
     */
    private void refreshBankInterest(Player player) {
        long balance = getBankMoney(player);
        int multiplier = calculateInterestMultiplier(player);

        boolean differentValues = plugin.getVariableManager().getDifferentValues();

        if (!differentValues) {
            //if standard is positiv, then there a positive interest
            //if standard is negative, then there a negative interest
            balance *= Math.pow(1 + plugin.getVariableManager().getStandardInterest(), multiplier);
        } else {


            if (multiplier < 1) return;

            long limitTier1 = plugin.getVariableManager().getLimitTier1();
            double interestTier1 = plugin.getVariableManager().getInterestTier1();

            long limitTier2 = plugin.getVariableManager().getLimitTier2();
            double interestTier2 = plugin.getVariableManager().getInterestTier2();

            long limitTier3 = plugin.getVariableManager().getLimitTier3();
            double interestTier3 = plugin.getVariableManager().getInterestTier3();

            if (balance < limitTier1) {

                balance *= Math.pow(1 - interestTier1, multiplier);

            } else {
                double v = limitTier1 * Math.pow(1 - interestTier1, multiplier);
                if (balance < limitTier2) {
                    long lowerInterest = (long) v;
                    long mediumInterest = (long) ((balance - limitTier1) * Math.pow(1 - interestTier2, multiplier));
                    balance = lowerInterest + mediumInterest;

                } else {
                    long lowerInterest = (long) v;
                    long mediumInterest = (long) ((limitTier2 - limitTier1) * Math.pow(1 - interestTier2, multiplier));
                    long higherInterest = (long) ((balance - limitTier2) * Math.pow(1 - interestTier3, multiplier));
                    balance = lowerInterest + mediumInterest + higherInterest;

                }
            }
        }

        player.sendMessage(plugin.getMessageManager().getInterestCharges(getBankMoney(player) - balance));

        setMoneyToBank(player, balance);
        setTimeBank(player);

        assert balance == getBankMoney(player);


    }

    private void setTimeBank(Player player) {
        fileConfig.set("players." + player.getUniqueId() + ".time", System.currentTimeMillis());
        fileConfig.saveConfig();
    }

    private int calculateInterestMultiplier(Player player) {
        long timeElapsedInHours = getBankTimeElapsed(player);
        int interval = plugin.getVariableManager().getInterestInterval();
        int maxMultiplier = plugin.getVariableManager().getMaxMultiplier();


        if (timeElapsedInHours < interval) {
            return 0;
        }

        return Math.min((int) (timeElapsedInHours / interval), maxMultiplier);
    }


    //Handle credit

    public void setCredit(Player player, Credit credit) {
        fileConfig.set("players." + player.getUniqueId() + ".credit", credit.getAmount());
        fileConfig.set("players." + player.getUniqueId() + ".creditType", credit.toString());

        fileConfig.saveConfig();
        long amount = getBankMoney(player);

        setCreditTime(player);
        setMoneyToBank(player, credit.getAmount() + amount);

        if (credit != Credit.NONE)
            player.sendMessage(plugin.getMessageManager().getGetCredit(credit.getAmount()));

    }

    private void setCreditTime(Player player) {
        fileConfig.set("players." + player.getUniqueId() + ".creditTime", System.currentTimeMillis());
        fileConfig.saveConfig();
    }

    public long calculateCredit(Player player) {

        long calculatedAmount = 0;
        if (playerHasCredit(player)) {
            long time = getCreditTime(player);
            long timeElapsed = time > 48 ? 48 : time;

            long originalCost = getOriginalCredit(player);

            calculatedAmount = (long) (originalCost * Math.pow(1 + getCreditInterest(), timeElapsed));


        }

        return calculatedAmount;

    }

    private long getOriginalCredit(Player player) {
        String creditType = getCreditType(player);

        switch (creditType) {
            case "IRON":
                return Credit.IRON.getAmount();
            case "GOLD":
                return Credit.GOLD.getAmount();
            case "REDSTONE":
                return Credit.REDSTONE.getAmount();
            case "DIAMOND":
                return Credit.DIAMOND.getAmount();
        }
        return 0;
    }


    private void setCreditInit(Player player) {
        fileConfig.set("players." + player.getUniqueId() + ".creditType", Credit.NONE.toString());

        fileConfig.saveConfig();
    }

    public String getCreditType(Player player) {
        return fileConfig.getString("players." + player.getUniqueId() + ".creditType");
    }


    private long getCreditTime(Player player) {
        long getCurrentTime = System.currentTimeMillis();
        long getTime = fileConfig.getLong("players." + player.getUniqueId() + ".creditTime");
        long inHours = (getCurrentTime - getTime) / 3600000;
        return inHours;
    }

    public boolean playerHasCredit(Player player) {
        return !fileConfig.getString("players." + player.getUniqueId() + ".creditType").equals(Credit.NONE.toString());
    }


    /**
     * Gets the players credit interest
     *
     * @return Interest of player
     */
    private double getCreditInterest() {
        return 0.01;

    }


    public void payBackCredit(Player player) {
        if (playerHasCredit(player)) {
            long amount = calculateCredit(player);

            if (getBankMoney(player) >= amount) {
                removeMoneyFromBank(player, amount);
                setCredit(player, Credit.NONE);
                setAccountFrozen(player, false);
                player.sendMessage(plugin.getMessageManager().getPayBackCredit());

                return;
            } else if (getPurseMoney(player) >= amount) {
                removePurseBalance(player, amount);
                setCredit(player, Credit.NONE);
                setAccountFrozen(player, false);
                player.sendMessage(plugin.getMessageManager().getPayBackCredit());

                return;
            }
            plugin.getMessageManager().getNotEnoughMoney();
        }

    }

    private void calculateFrozen(Player player) {
        //If Player hasn't paid back his credit in 72 hours, his account will be frozen

        if (playerHasCredit(player) && getCreditTime(player) > 72) {
            setAccountFrozen(player, true);
            player.sendMessage(plugin.getMessageManager().getIsFrozen());
        }
    }

    private void setAccountFrozen(Player player, boolean value) {
        fileConfig.set("players." + player.getUniqueId() + ".frozen", value);
        fileConfig.saveConfig();
    }

    public boolean isAccountFrozen(Player player) {
        return fileConfig.getBoolean("players." + player.getUniqueId() + ".frozen");
    }

    public enum Credit {

        NONE(0),
        IRON(5000),
        GOLD(10000),
        REDSTONE(25000),
        DIAMOND(50000);


        final long amount;

        Credit(int i) {
            this.amount = i;
        }

        public long getAmount() {

            return amount;
        }
    }

}
