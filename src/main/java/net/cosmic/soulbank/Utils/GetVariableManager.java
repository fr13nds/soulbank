package net.cosmic.soulbank.Utils;

import net.cosmic.soulbank.SoulBank;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

public class GetVariableManager {

    private final FileConfiguration config;
    private int interestInterval;
    private int maxMultiplier;
    private boolean differentValues;
    private double standardInterest;
    private long limitTier1;
    private double interestTier1;
    private long limitTier2;
    private double interestTier2;
    private long limitTier3;
    private double interestTier3;

    public GetVariableManager(SoulBank instance) {
        this.config = instance.getConfig();
        reader();

    }

    public void reader() {
        try {
            interestInterval = config.getInt("interestInterval");
            maxMultiplier = config.getInt("maxMultiplier");
            differentValues = config.getBoolean("differentValues");
            standardInterest = config.getDouble("standardInterest");

            if (differentValues) {
                limitTier1 = config.getLong("limitTier1");
                interestTier1 = config.getDouble("interestTier1");
                limitTier2 = config.getLong("limitTier2");
                interestTier2 = config.getDouble("interestTier2");
                limitTier3 = config.getLong("limitTier3");
                interestTier3 = config.getDouble("interestTier3");

                if(interestInterval < 1 && maxMultiplier < 1){
                    Bukkit.getLogger().log(java.util.logging.Level.SEVERE, "[SoulBank] InterestInterval or maxMultiplier is less than 1");

                    return;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getInterestInterval() {
        return interestInterval;
    }

    public int getMaxMultiplier() {
        return maxMultiplier;
    }

    public boolean getDifferentValues() {
        return differentValues;
    }

    public double getStandardInterest() {
        return standardInterest;
    }

    public long getLimitTier1() {
        return limitTier1;
    }

    public double getInterestTier1() {
        return interestTier1;
    }

    public long getLimitTier2() {
        return limitTier2;
    }

    public double getInterestTier2() {
        return interestTier2;
    }

    public long getLimitTier3() {
        return limitTier3;
    }

    public double getInterestTier3() {
        return interestTier3;
    }



}

