package net.cosmic.soulbank.Utils;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class ConstructTabComplete implements TabCompleter {
    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        if(command.getName().equals("soulbank")){
            if(args.length == 1 && sender.hasPermission("soulbank.admin")) {
                return List.of("balance", "deposit", "withdraw", "payback", "help", "version");
            }
            else if(args.length == 1  && sender.hasPermission("soulbank.text")){
                return List.of("balance", "deposit", "withdraw", "payback", "help");
            }
        }




        return null;
    }
}
