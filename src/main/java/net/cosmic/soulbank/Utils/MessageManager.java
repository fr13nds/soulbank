package net.cosmic.soulbank.Utils;

import net.cosmic.soulbank.SoulBank;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class MessageManager {

    private String name;
    private String prefix;
    private String withdrawAmount;
    private String depositAmount;
    private String notEnoughMoney;
    private String isFrozen;
    private String payBackCredit;
    private String getCredit;
    private String nothingToWithdraw;
    private String nothingToDeposit;
    private String onlyNumbers;
    private String interestCharges;


    private final FileConfiguration config;

    public MessageManager(SoulBank instance) {
        this.config = instance.getConfig();
        reader();
    }

    public void reader() {
        try {
            name = config.getString("name").replace("'", "");
            prefix = config.getString("prefix").replace("'", "");
            withdrawAmount = config.getString("messages.withdrawAmount").replace("'", "");
            depositAmount = config.getString("messages.depositAmount").replace("'", "");
            notEnoughMoney = config.getString("messages.notEnoughMoney").replace("'", "");
            isFrozen = config.getString("messages.isFrozen").replace("'", "");
            payBackCredit = config.getString("messages.payBackCredit").replace("'", "");
            getCredit = config.getString("messages.getCredit").replace("'", "");
            nothingToWithdraw = config.getString("messages.nothingToWithdraw").replace("'", "");
            nothingToDeposit = config.getString("messages.nothingToDeposit").replace("'", "");
            onlyNumbers = config.getString("messages.onlyNumbers").replace("'", "");
            interestCharges = config.getString("messages.interestCharges").replace("'", "");


            System.out.println("[SoulBank] Successfully loaded messages");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    public String getWithdrawAmount(long amount) {
        return prefix + withdrawAmount.replace("{amount}", String.valueOf(amount));
    }

    public String getDepositAmount(long amount) {
        return prefix + depositAmount.replace("{amount}", String.valueOf(amount));
    }

    public String getNotEnoughMoney() {
        return prefix + notEnoughMoney;
    }

    public String getIsFrozen() {
        return prefix + isFrozen;
    }

    public String getPayBackCredit() {
        return prefix + payBackCredit;
    }

    public String getGetCredit(long amount) {
        return prefix + getCredit.replace("{amount}", String.valueOf(amount));
    }

    public String getName() {
        return name;
    }

    public String getNothingToWithdraw() {
        return prefix + nothingToWithdraw;
    }

    public String getNothingToDeposit() {
        return prefix + nothingToDeposit;
    }

    public String getOnlyNumbers() {
        return prefix + onlyNumbers;
    }

    public String getInterestCharges(long amount) {
        return prefix + interestCharges.replace("{amount}", String.valueOf(amount));
    }


    public String getVersion() {
        return prefix + "§cVersion: §c§l" + SoulBank.getInstance().getDescription().getVersion();
    }

    public String getHelp() {
        //Should show all commands and their description
        String seperator = "§7§m-------------------------\n";
        String help = prefix + "§c§lHelp\n";

        String deposit = "§c§l/soulbank deposit <amount> §7§l- Deposits the specified amount of money\n";
        String withdraw = "§c§l/soulbank withdraw <amount> §7§l- Withdraws the specified amount of money\n";
        String balance = "§c§l/soulbank balance §7§l- Shows your current balance\n";
        String payback = "§c§l/soulbank payback §7§l- Pays back your credit\n";

        return seperator + help + deposit + withdraw + balance + payback + seperator;
    }

    public String getBalance(Player player) {
        return prefix + "§a§lBalance: §a§l" + SoulBank.getInstance().getFileConfigManager().getBankMoney(player) + "§a€";
    }


    public String noPermission() {
        return prefix + "§c§lYou don't have the permission to do that!";
    }

    public String getNoCredit() {
        return prefix + "§c§lYou don't have a credit!";
    }

}
