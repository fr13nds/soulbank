package net.cosmic.soulbank.Utils;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

public class SkullCreator {


    public static ItemStack createRealPlayerSkull(Player player, String... lore) {

        ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta sm = (SkullMeta) skull.getItemMeta();

        sm.setDisplayName("§6§l"+ player.getPlayer().getName());
        sm.setLore(Arrays.asList(lore));
        sm.setOwningPlayer(player);

        skull.setItemMeta(sm);

        return skull;
    }
}
