package net.cosmic.soulbank;

import com.earth2me.essentials.Essentials;
import mc.obliviate.inventory.InventoryAPI;
import net.cosmic.soulbank.Commands.OpenBank;
import net.cosmic.soulbank.FileConfig.FileConfigManager;
import net.cosmic.soulbank.Listeners.onJoin;
import net.cosmic.soulbank.Utils.ConstructTabComplete;
import net.cosmic.soulbank.Utils.GetVariableManager;
import net.cosmic.soulbank.Utils.MessageManager;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public final class SoulBank extends JavaPlugin {

    private Essentials essentials;
    private FileConfigManager fileConfigManager;

    private MessageManager messageManager;

    private GetVariableManager variableManager;

    public static SoulBank getInstance() {
        return (SoulBank) getPlugin(SoulBank.class);
    }
    @Override
    public void onEnable() {
        // Plugin startup logic

        essentials = (Essentials) getServer().getPluginManager().getPlugin("Essentials");
        fileConfigManager = new FileConfigManager(essentials);

        new InventoryAPI(this).init();

        messageManager = new MessageManager(this);
        variableManager = new GetVariableManager(this);

        listenerRegister();

        this.saveDefaultConfig();



        this.getCommand("soulbank").setExecutor(new OpenBank());
        this.getCommand("soulbank").setTabCompleter(new ConstructTabComplete());


    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }


    public void listenerRegister(){
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new onJoin(), this);
    }

    public FileConfigManager getFileConfigManager() {
        return fileConfigManager;
    }

    public MessageManager getMessageManager() {
        return messageManager;
    }

    public GetVariableManager getVariableManager() {
        return variableManager;
    }
}
