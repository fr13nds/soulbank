package net.cosmic.soulbank.Commands;

import net.cosmic.soulbank.GUI.MainMenu;
import net.cosmic.soulbank.SoulBank;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class OpenBank implements CommandExecutor {
    SoulBank plugin = SoulBank.getPlugin(SoulBank.class);

    /**
     * This is the command that will be used to open the Bank per command
     * @param commandSender The sender of the command.
     * @param command The command that was sent.
     * @param s The label of the command.
     * @param strings The arguments of the command.
     * @return Whether the command was successful.
     */
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player))
            return false;

        Player player = (Player) commandSender;

        plugin.getFileConfigManager().initBank(player);
        plugin.getFileConfigManager().refreshBank(player);


        if(strings.length == 0 && player.hasPermission("soulbank.admin")){
            new MainMenu(player).open();
            return true;
        }


        //Chat Based Commands

        if(player.hasPermission("soulbank.text")) {
            //Bank Balance Command
            switch (strings[0]) {
                case "balance":
                    player.sendMessage(plugin.getMessageManager().getBalance(player));
                    return true;

                //Deposit Command
                case "deposit":
                    if (strings.length == 2) {
                        if (strings[1].matches("[0-9]+")) {
                            plugin.getFileConfigManager().addMoneyToBank(Integer.parseInt(strings[1]), player);
                            return true;
                        }

                    }
                    break;

                //Withdraw Command
                case "withdraw":
                    if (strings.length == 2) {
                        if (strings[1].matches("[0-9]+")) {
                            plugin.getFileConfigManager().withdrawFromBank(player, Integer.parseInt(strings[1]));
                            return true;
                        }

                    }
                    break;

                //Pay Back Command
                case "payback":
                    plugin.getFileConfigManager().payBackCredit(player);

                    break;//Help Command

                case "version":
                    if (!player.hasPermission("soulbank.admin"))
                        return true;
                    player.sendMessage(plugin.getMessageManager().getVersion());
                    return true;


                default:
                    player.sendMessage(plugin.getMessageManager().getHelp());
                    return true;
            }
        }
        player.sendMessage(plugin.getMessageManager().noPermission());

        return true;


    }
}
